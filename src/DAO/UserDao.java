package DAO;

import Conexao.ConnectionFactory;
import classes.Ratings;
import classes.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {

    private int id;
    private Connection connection;

    public UserDao() {
        this.connection = new ConnectionFactory().getConnection();
    }

    public void adiciona(User user) { //Função irá adicionar um novo usuário no banco
        String sql = "insert into user "
                + "(login,senha)"
                + " values (?,?)";
        try ( // Enviar a query com o inserte pre-compilado
                PreparedStatement stmt = connection.prepareStatement(sql)) {
            // parameteriza os valores do insert
            stmt.setString(1, user.getLogin());
            stmt.setString(2, user.getSenha());
            // executa
            stmt.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /*
    Metodo irá verificar o login e senha e retornar verdadeiro se ambos estiverem
    cadastrados no banco.
     */
    public boolean checkLogin(String login, String senha) throws SQLException {
        boolean check = false;
        try (Connection con = new ConnectionFactory().getConnection()) { //Tenta abrir a conexão
            String sql = "select * from user where login = ? and senha = ?"; //Select irá pegar todos os usuários
            try (PreparedStatement stmt = con.prepareStatement(sql);) { //Tenta enviar a query para o pre-compilamento
                // parameteriza os valores do insert
                stmt.setString(1, login);
                stmt.setString(2, senha);
                ResultSet rs = stmt.executeQuery(); // Pega o resultado da execução da query
                while (rs.next()) { //Varre todos os registro retornados pela query
                    if (rs.getString("login").equals(login) && rs.getString("senha").equals(senha)) { //Compara o login e senha com o retorno da linha atual da query
                        check = true;
                    }
                }
            }
        }
        return check;
    }
    /*
    Verifica se já existe login cadastrado
    */
    public boolean AchouUser(String login) throws SQLException {
        boolean check = false;
        try (Connection con = new ConnectionFactory().getConnection()) { //Abre conexão com o banco
            String sql = "select * from user where login='" + login + "'"; //Cria String com a Query
            try (PreparedStatement stmt = con.prepareStatement(sql);) { //Tenta enviar a query para o pre-compilamento
                ResultSet rs = stmt.executeQuery(); //Executa e pega o retorno na query
                while (rs.next()) { //Verifica todas as linha do retorno da query
                    if (rs.getString("login").equals(login)) {
                        check = true;
                    }
                }
            }
        }
        return check;
    }

    /*
    Metodo irá retornar o id do usuário com base no seu login
    */
    public int recuperaId(String login) throws SQLException {
        Connection con = new ConnectionFactory().getConnection();
        String sql = "select * from user where login='" + login + "'";
        PreparedStatement stmt = con.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        if (rs != null && rs.next()) {
            this.id = rs.getInt("idUser");
        }

        return id;
    }
}
