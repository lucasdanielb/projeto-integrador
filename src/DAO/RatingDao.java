package DAO;

import Conexao.ConnectionFactory;
import classes.Ratings;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RatingDao {

    private Connection connection;

    public RatingDao() {
        this.connection = new ConnectionFactory().getConnection();
    }

    public void adiciona(Ratings rate) {
        String sql = "insert into ratings "
                + "(userid,movieid,rating, timestamp)"
                + " values (?,?,?, 1217897793)"; //String com a query sql
        try {
            // Enviar a query com o inserte pre-compilado
            PreparedStatement stmt = connection.prepareStatement(sql);
            // parameteriza os valores do insert
            stmt.setInt(1, rate.getUserid());
            stmt.setInt(2, rate.getMovieid());
            stmt.setInt(3, rate.getRating());
            stmt.execute(); // executa o insert
            stmt.close(); //fecha conexão
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
