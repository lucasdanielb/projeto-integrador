package DAO;

import Conexao.ConnectionFactory;
import classes.Movie;
import classes.Ratings;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MovieDao {

    private Connection connection;

    public MovieDao() { 
        this.connection = new ConnectionFactory().getConnection(); //irá abrir conexão no momento da instanciação da classe MovieDao
    }

    public void adiciona(Movie movie) {
        String sql = "insert into movies "
                + "(id,nome,genero)"
                + " values (?,?,?,?)"; //String com o insert sql
        try {
            // Enviar a query com o inserte pre-compilado
            PreparedStatement stmt = connection.prepareStatement(sql);
            // parameteriza os valores do insert
            stmt.setInt(1, movie.getId());
            stmt.setString(2, movie.getNome());
            stmt.setString(3, movie.getGenero());
            stmt.execute(); //executa o insert
            stmt.close(); //fecha conexão
        } catch (SQLException e) { //executa caso ocorre algum erro no bloco anterior
            throw new RuntimeException(e);
        }
    }
}
