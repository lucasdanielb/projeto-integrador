/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import DAO.UserDao;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class index extends javax.swing.JFrame {

    private static int idLogado = -1;
    
    public static int getIdLogado() {
        return idLogado;
    }

   
    private static void makeFrameFullSize(JFrame aFrame) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        aFrame.setSize(screenSize.width, screenSize.height);
    }

    public index() {
        initComponents();
        //URL url = this.getClass().getResource("//icone.png");
        //Image iconeTitulo = Toolkit.getDefaultToolkit().getImage(url);
        //this.setIconImage(iconeTitulo);
        //Color redColor = new Color(204,204,255);
        this.getContentPane().setBackground(Color.white);
       // this.idLogado = idLogado;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CampoLogin = new javax.swing.JTextField();
        CampoSenha = new javax.swing.JPasswordField();
        BotaoEntrar = new javax.swing.JButton();
        BotaoCadastrar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Pitakos Filmes - Seja Bem Vindo !");
        setMinimumSize(new java.awt.Dimension(1024, 768));
        setPreferredSize(new java.awt.Dimension(1390, 700));
        getContentPane().setLayout(null);

        CampoLogin.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        CampoLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CampoLoginActionPerformed(evt);
            }
        });
        getContentPane().add(CampoLogin);
        CampoLogin.setBounds(430, 10, 212, 30);

        CampoSenha.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        getContentPane().add(CampoSenha);
        CampoSenha.setBounds(720, 10, 210, 30);

        BotaoEntrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        BotaoEntrar.setText("Entrar");
        BotaoEntrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoEntrarActionPerformed(evt);
            }
        });
        getContentPane().add(BotaoEntrar);
        BotaoEntrar.setBounds(950, 10, 80, 30);

        BotaoCadastrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        BotaoCadastrar.setText("Cadastrar");
        BotaoCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoCadastrarActionPerformed(evt);
            }
        });
        getContentPane().add(BotaoCadastrar);
        BotaoCadastrar.setBounds(1040, 10, 110, 30);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Login:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(380, 10, 50, 17);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Senha:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(663, 10, 60, 17);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/filmes.png"))); // NOI18N
        jLabel3.setText("jLabel3");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(140, 10, 100, 30);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/pi.png"))); // NOI18N
        jLabel5.setText("jLabel5");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(0, 10, 140, 30);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/fundofinal.jpg"))); // NOI18N
        jLabel4.setText("jLabel4");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, 50, 1520, 690);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BotaoEntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoEntrarActionPerformed
        String login = CampoLogin.getText();
        String senha = CampoSenha.getText();
        if (!login.equals("") && !senha.equals("")) {
            UserDao teste = new UserDao();
            try {
                if (teste.checkLogin(login, senha)) {
                    Component frame = null;
                    JOptionPane.showMessageDialog(frame,
                            "Logado com sucesso !");
                    this.idLogado = teste.recuperaId(login);
                    System.out.println(idLogado);
                    home h = new home();//new home(idLogado);
                    h.setVisible(true);
                    this.dispose();
                } else {
                    Component frame = null;
                    JOptionPane.showMessageDialog(frame,
                            "Login ou senha incorretos !");
                }
            } catch (SQLException ex) {
                Logger.getLogger(index.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                teste.recuperaId(login);
            } catch (SQLException ex) {
                Logger.getLogger(index.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Component frame = null;
            JOptionPane.showMessageDialog(frame, "Preencha corretamente os campos !");
        }

    }//GEN-LAST:event_BotaoEntrarActionPerformed

    private void CampoLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CampoLoginActionPerformed

    }//GEN-LAST:event_CampoLoginActionPerformed

    private void BotaoCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoCadastrarActionPerformed
        new Cadastro().setVisible(true);
    }//GEN-LAST:event_BotaoCadastrarActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame index = new index();
                makeFrameFullSize(index);
                index.setVisible(true);
                 }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotaoCadastrar;
    private javax.swing.JButton BotaoEntrar;
    private javax.swing.JTextField CampoLogin;
    private javax.swing.JPasswordField CampoSenha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}
