package view;

import Conexao.ConnectionFactory;
import DAO.RatingDao;
import classes.Movie;
import classes.Ratings;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class home extends javax.swing.JFrame {

    int idLogado = index.getIdLogado();

    public home() throws SQLException {
        initComponents();
        Color redColor = new Color(51, 63, 158);
        this.getContentPane().setBackground(redColor);
        this.getContentPane().setBackground(redColor);
        this.preencheMelhores();
        this.preencheComboUltimos();
        this.preencheComboUltimosAll();
        System.out.println(idLogado);

    }

    private static void makeFrameFullSize(JFrame aFrame) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        aFrame.setSize(screenSize.width, screenSize.height);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenu2 = new javax.swing.JMenu();
        jPopupMenu2 = new javax.swing.JPopupMenu();
        LabelMelhores = new javax.swing.JLabel();
        LabelIndicacoes = new javax.swing.JLabel();
        LabelNotas = new javax.swing.JLabel();
        ComboFilmes = new javax.swing.JComboBox<>();
        ComboNotas = new javax.swing.JComboBox<>();
        BotaoOk = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        ListaMelhores = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        ListaMelhores1 = new javax.swing.JList<>();
        CheckAction = new javax.swing.JCheckBox();
        CheckAdventure = new javax.swing.JCheckBox();
        CheckAnimation = new javax.swing.JCheckBox();
        CheckChildren = new javax.swing.JCheckBox();
        CheckComedy = new javax.swing.JCheckBox();
        CheckCrime = new javax.swing.JCheckBox();
        CheckDocumentary = new javax.swing.JCheckBox();
        CheckDrama = new javax.swing.JCheckBox();
        CheckFantasy = new javax.swing.JCheckBox();
        CheckHorror = new javax.swing.JCheckBox();
        CheckRomance = new javax.swing.JCheckBox();
        CheckWar = new javax.swing.JCheckBox();
        ButtonProcurar = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        ListaRecomendados = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        ListaUltimos = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        ListaUltimos1 = new javax.swing.JList<>();
        jLabel3 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        jMenu2.setText("jMenu2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Pitakos Filmes\n");
        setBackground(new java.awt.Color(204, 204, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setFocusTraversalPolicyProvider(true);
        setForeground(new java.awt.Color(0, 0, 0));
        setMinimumSize(new java.awt.Dimension(1370, 768));
        setName("home"); // NOI18N
        getContentPane().setLayout(null);

        LabelMelhores.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LabelMelhores.setForeground(new java.awt.Color(255, 255, 255));
        LabelMelhores.setText("Melhores Filmes");
        getContentPane().add(LabelMelhores);
        LabelMelhores.setBounds(50, 190, 238, 41);

        LabelIndicacoes.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LabelIndicacoes.setForeground(new java.awt.Color(255, 255, 255));
        LabelIndicacoes.setText("Filmes Indicados Para Você");
        getContentPane().add(LabelIndicacoes);
        LabelIndicacoes.setBounds(50, 420, 261, 41);

        LabelNotas.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LabelNotas.setForeground(new java.awt.Color(255, 255, 255));
        LabelNotas.setText("Dê Sua Nota");
        getContentPane().add(LabelNotas);
        LabelNotas.setBounds(50, 70, 130, 41);

        ComboFilmes.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        getContentPane().add(ComboFilmes);
        ComboFilmes.setBounds(190, 150, 620, 40);

        ComboNotas.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        ComboNotas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5" }));
        getContentPane().add(ComboNotas);
        ComboNotas.setBounds(830, 150, 90, 40);

        BotaoOk.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        BotaoOk.setText("Ok");
        BotaoOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaoOkActionPerformed(evt);
            }
        });
        getContentPane().add(BotaoOk);
        BotaoOk.setBounds(950, 150, 90, 40);

        jButton3.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jButton3.setText("Atualizar Indicações");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3);
        jButton3.setBounds(320, 430, 170, 23);

        ListaMelhores.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        ListaMelhores.setDoubleBuffered(true);
        ListaMelhores.setDragEnabled(true);
        ListaMelhores.setFocusCycleRoot(true);
        ListaMelhores.setFocusTraversalPolicyProvider(true);
        ListaMelhores.setInheritsPopupMenu(true);
        jScrollPane1.setViewportView(ListaMelhores);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(50, 230, 1000, 180);

        jScrollPane2.setViewportView(ListaMelhores1);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(50, 230, 1000, 180);

        CheckAction.setBackground(new java.awt.Color(255, 255, 255));
        CheckAction.setForeground(new java.awt.Color(255, 255, 255));
        CheckAction.setText("Action");
        CheckAction.setOpaque(false);
        getContentPane().add(CheckAction);
        CheckAction.setBounds(200, 80, 100, 20);

        CheckAdventure.setBackground(new java.awt.Color(255, 255, 255));
        CheckAdventure.setForeground(new java.awt.Color(255, 255, 255));
        CheckAdventure.setText("Adventure");
        CheckAdventure.setAlignmentY(0.0F);
        CheckAdventure.setOpaque(false);
        getContentPane().add(CheckAdventure);
        CheckAdventure.setBounds(200, 110, 120, 20);

        CheckAnimation.setBackground(new java.awt.Color(255, 255, 255));
        CheckAnimation.setForeground(new java.awt.Color(255, 255, 255));
        CheckAnimation.setText("Animation");
        CheckAnimation.setOpaque(false);
        getContentPane().add(CheckAnimation);
        CheckAnimation.setBounds(330, 80, 130, 23);

        CheckChildren.setBackground(new java.awt.Color(255, 255, 255));
        CheckChildren.setForeground(new java.awt.Color(255, 255, 255));
        CheckChildren.setText("Children");
        CheckChildren.setOpaque(false);
        getContentPane().add(CheckChildren);
        CheckChildren.setBounds(330, 110, 140, 23);

        CheckComedy.setBackground(new java.awt.Color(255, 255, 255));
        CheckComedy.setForeground(new java.awt.Color(255, 255, 255));
        CheckComedy.setText("Comedy");
        CheckComedy.setOpaque(false);
        getContentPane().add(CheckComedy);
        CheckComedy.setBounds(470, 80, 120, 23);

        CheckCrime.setBackground(new java.awt.Color(255, 255, 255));
        CheckCrime.setForeground(new java.awt.Color(255, 255, 255));
        CheckCrime.setText("Crime");
        CheckCrime.setOpaque(false);
        getContentPane().add(CheckCrime);
        CheckCrime.setBounds(470, 110, 110, 23);

        CheckDocumentary.setBackground(new java.awt.Color(255, 255, 255));
        CheckDocumentary.setForeground(new java.awt.Color(255, 255, 255));
        CheckDocumentary.setText("Documentary");
        CheckDocumentary.setOpaque(false);
        getContentPane().add(CheckDocumentary);
        CheckDocumentary.setBounds(590, 80, 120, 23);

        CheckDrama.setBackground(new java.awt.Color(255, 255, 255));
        CheckDrama.setForeground(new java.awt.Color(255, 255, 255));
        CheckDrama.setText("Drama");
        CheckDrama.setOpaque(false);
        getContentPane().add(CheckDrama);
        CheckDrama.setBounds(590, 110, 130, 23);

        CheckFantasy.setBackground(new java.awt.Color(255, 255, 255));
        CheckFantasy.setForeground(new java.awt.Color(255, 255, 255));
        CheckFantasy.setText("Fantasy");
        CheckFantasy.setOpaque(false);
        getContentPane().add(CheckFantasy);
        CheckFantasy.setBounds(720, 80, 120, 23);

        CheckHorror.setBackground(new java.awt.Color(255, 255, 255));
        CheckHorror.setForeground(new java.awt.Color(255, 255, 255));
        CheckHorror.setText("Horror");
        CheckHorror.setOpaque(false);
        getContentPane().add(CheckHorror);
        CheckHorror.setBounds(720, 110, 120, 23);

        CheckRomance.setBackground(new java.awt.Color(255, 255, 255));
        CheckRomance.setForeground(new java.awt.Color(255, 255, 255));
        CheckRomance.setText("Romance");
        CheckRomance.setOpaque(false);
        getContentPane().add(CheckRomance);
        CheckRomance.setBounds(840, 80, 90, 23);

        CheckWar.setBackground(new java.awt.Color(255, 255, 255));
        CheckWar.setForeground(new java.awt.Color(255, 255, 255));
        CheckWar.setText("War");
        CheckWar.setOpaque(false);
        getContentPane().add(CheckWar);
        CheckWar.setBounds(840, 110, 90, 23);

        ButtonProcurar.setFont(new java.awt.Font("Arial", 1, 11)); // NOI18N
        ButtonProcurar.setText("Procurar");
        ButtonProcurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonProcurarActionPerformed(evt);
            }
        });
        getContentPane().add(ButtonProcurar);
        ButtonProcurar.setBounds(950, 80, 90, 50);

        jScrollPane3.setViewportView(ListaRecomendados);

        getContentPane().add(jScrollPane3);
        jScrollPane3.setBounds(50, 470, 1000, 190);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/logo.png"))); // NOI18N
        jLabel1.setAutoscrolls(true);
        getContentPane().add(jLabel1);
        jLabel1.setBounds(1160, 0, 210, 80);

        jScrollPane4.setViewportView(ListaUltimos);

        getContentPane().add(jScrollPane4);
        jScrollPane4.setBounds(1090, 430, 250, 240);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Últimos Filmes Avaliados");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(1120, 390, 190, 40);

        jScrollPane5.setViewportView(ListaUltimos1);

        getContentPane().add(jScrollPane5);
        jScrollPane5.setBounds(1090, 140, 250, 240);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Últimos Filmes Avaliados Por Você");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(1090, 100, 260, 40);

        jMenuBar1.setPreferredSize(new java.awt.Dimension(39, 15));

        jMenu3.setText("Ajuda");

        jMenuItem1.setText("Manual do usuário");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem1);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    /*
    Botão que envia avaliação do usuario
     */
    private void BotaoOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaoOkActionPerformed
        try {
            avaliarFilme();
        } catch (SQLException ex) {
            Logger.getLogger(home.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BotaoOkActionPerformed
    /*
    Botão irá buscar os filmes para avaliação de acordo com as categorias selecionadas.
     */
    private void ButtonProcurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonProcurarActionPerformed
        try {
            this.preencheComboFilmes();
        } catch (SQLException ex) {
            Logger.getLogger(home.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ButtonProcurarActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        try {
            String dir = System.getProperty("user.dir");
            String cam = dir + "\\";
            Runtime.getRuntime().exec("cmd /c start \"\"  \"" + cam + "help.chm\"");

        } catch (Exception erro) {
            JOptionPane.showMessageDialog(null, erro);
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void CheckActionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckActionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CheckActionActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        String sql = "call FilmesIndicados(" + this.idLogado + ")"; //String que
        CallableStatement stmt;
        Connection con = new ConnectionFactory().getConnection(); //Abre conexão
        try {
            stmt = con.prepareCall(sql); // Prepara query para pré-compilação
            ResultSet rs = stmt.executeQuery(); //Executa query
            if (rs != null && rs.next()) {
                DefaultListModel model = new DefaultListModel();
                while (rs.next()) {
                    String ItemList2 = rs.getString("titulo"); //Pega o titulo do Filme
                    model.addElement(ItemList2); //Adiciona o Filme a Lista DefaultListModel

                }
                ListaRecomendados.setModel(model); //Passa todos os Filmes para a ListaRecomendados
                rs.close(); //Fecha conexão
            }

        } catch (SQLException ex) {
            Logger.getLogger(home.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_jButton3ActionPerformed

    public void preencheComboFilmes() throws SQLException {
        //Faz um cast do model do JComboBox
        DefaultComboBoxModel comboModelFilmes = (DefaultComboBoxModel) ComboFilmes.getModel();

        //cria a lista: java.util.List
        List<Movie> movie = new ArrayList<Movie>();

        comboModelFilmes.removeAllElements(); //Remove todos os elementos do comboFilmes
        try (Connection con = new ConnectionFactory().getConnection()) {
            String sql = buildQuery();
            try (PreparedStatement stmt = con.prepareStatement(sql); ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    movie.add(new Movie(rs.getInt("id"), rs.getString("titulo"), ""));
                }

                //percorrendo a lista para inserir os valores no combo
                for (int linha = 0; linha < movie.size(); linha++) {
                    //pegando a categoria da lista
                    Movie movieAtual = movie.get(linha);
                    //adicionando a categoria no combo
                    comboModelFilmes.addElement(movieAtual);
                }
            }
        }
        ComboFilmes.repaint();

    }

    /*
    Preenche o combo ListaUltimos1 com os ultimos filmes avaliados pelo usuario logado. 
    Pega apenas os 20 primeiros
     */
    public void preencheComboUltimos() throws SQLException {
        DefaultListModel model = new DefaultListModel();
        try (Connection con = new ConnectionFactory().getConnection()) {
            String sql = "SELECT movies.titulo,ratings.movieid,ratings.codigo FROM \n" //Query irá pegar os 20 ultimos filmes avaliados pelo usuario
                    + "	movies JOIN ratings ON movies.id = ratings.movieid \n"
                    + "WHERE ratings.userid = " + this.idLogado
                    + " ORDER BY ratings.codigo desc LIMIT 20";
            try (PreparedStatement stmt = con.prepareStatement(sql); ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String ItemList2 = rs.getString("titulo");
                    model.addElement(ItemList2);
                }
                rs.close();
            }
            ListaUltimos1.setModel(model);

        }

    }
/*
    Preenche o combo ListaUltimos1 com os ultimos filmes avaliados pelo usuario logado, neste metodo pega todos
     */
    public void preencheComboUltimosAll() throws SQLException {
        DefaultListModel model = new DefaultListModel();
        try (Connection con = new ConnectionFactory().getConnection()) {
            String sql = "SELECT movies.titulo,ratings.movieid,ratings.codigo FROM \n"
                    + "	movies JOIN ratings ON movies.id = ratings.movieid \n";
            try (PreparedStatement stmt = con.prepareStatement(sql); ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    String ItemList2 = rs.getString("titulo");
                    model.addElement(ItemList2);
                }
                rs.close();
            }
            ListaUltimos.setModel(model);

        }

    }

    public void preencheMelhores() throws SQLException {
        DefaultListModel model = new DefaultListModel();
        try (Connection con = new ConnectionFactory().getConnection()) {
            String sql = "SELECT movies.titulo,COUNT(ratings.movieid) \n"
                    + "	FROM ratings JOIN movies ON ratings.movieid = movies.id\n"
                    + "	WHERE rating = 5\n"
                    + "    GROUP BY movieid\n"
                    + "    HAVING COUNT(movieid) > 100\n"
                    + "    ORDER BY COUNT(movieid) DESC\n"
                    + "";

            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    String ItemList2 = rs.getString("titulo");
                    model.addElement(ItemList2);

                }

                ListaMelhores.setModel(model);
                rs.close();
            }
        }
    }

    void avaliarFilme() throws SQLException {
        RatingDao rat = new RatingDao();
        int nota = Integer.parseInt((String) ComboNotas.getSelectedItem());
        Movie filme = (Movie) ComboFilmes.getSelectedItem();

        if (ComboFilmes.getSelectedItem() != null) {
            Connection con = new ConnectionFactory().getConnection();

            PreparedStatement stmt = null;

            try {
                stmt = con.prepareStatement(buildQuery());
            } catch (SQLException ex) {
                Logger.getLogger(home.class.getName()).log(Level.SEVERE, null, ex);
            }
            ResultSet rs = null;
            try {
                rs = stmt.executeQuery();
            } catch (SQLException ex) {
                Logger.getLogger(home.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                if (rs != null && rs.next()) {
                    Ratings rate = new Ratings();
                    rate.setRating(nota);
                    rate.setMovieid(filme.getId());
                    rate.setUserid(index.getIdLogado());
                    RatingDao r = new RatingDao();
                    r.adiciona(rate);
                    Component frame = null;
                    JOptionPane.showMessageDialog(frame, "Nota salva com sucesso !");
                    //System.out.println(movieid);
                }
            } catch (SQLException ex) {
                Logger.getLogger(home.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        preencheComboUltimos();
    }
    /*
    Irá montar a string para pegar os Filmes que poderão ser avaliados
    */
    public String buildQuery() {
        ArrayList<String> lista = new ArrayList<>();
        String teste;
        String subquery;

        if (this.CheckAction.isSelected()) {
            lista.add("Action");
        }
        if (this.CheckAnimation.isSelected()) {
            lista.add("Animation");
        }
        if (this.CheckAdventure.isSelected()) {
            lista.add("Adventure");
        }
        if (this.CheckChildren.isSelected()) {
            lista.add("Children");
        }
        if (this.CheckComedy.isSelected()) {
            lista.add("Comedy");
        }
        if (this.CheckCrime.isSelected()) {
            lista.add("Crime");
        }
        if (this.CheckDocumentary.isSelected()) {
            lista.add("Documentary");
        }
        if (this.CheckFantasy.isSelected()) {
            lista.add("Fantasy");
        }
        if (this.CheckDrama.isSelected()) {
            lista.add("Drama");
        }
        if (this.CheckHorror.isSelected()) {
            lista.add("Horror");
        }
        if (this.CheckRomance.isSelected()) {
            lista.add("Romance");
        }
        if (this.CheckWar.isSelected()) {
            lista.add("War");
        }

        if (lista.isEmpty()) {
            teste = "select id, titulo from movies";
        } else {
            teste = "select id, titulo from movies where genero like" + "'%" + lista.get(0) + "%'";
            for (int i = 1; i < lista.size(); i++) {
                subquery = "and genero like '%" + lista.get(i) + "%'";
                teste += subquery;
            }

        }
        return teste + " order by titulo";

    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new home().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(home.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotaoOk;
    private javax.swing.JButton ButtonProcurar;
    private javax.swing.JCheckBox CheckAction;
    private javax.swing.JCheckBox CheckAdventure;
    private javax.swing.JCheckBox CheckAnimation;
    private javax.swing.JCheckBox CheckChildren;
    private javax.swing.JCheckBox CheckComedy;
    private javax.swing.JCheckBox CheckCrime;
    private javax.swing.JCheckBox CheckDocumentary;
    private javax.swing.JCheckBox CheckDrama;
    private javax.swing.JCheckBox CheckFantasy;
    private javax.swing.JCheckBox CheckHorror;
    private javax.swing.JCheckBox CheckRomance;
    private javax.swing.JCheckBox CheckWar;
    private javax.swing.JComboBox<String> ComboFilmes;
    private javax.swing.JComboBox<String> ComboNotas;
    private javax.swing.JLabel LabelIndicacoes;
    private javax.swing.JLabel LabelMelhores;
    private javax.swing.JLabel LabelNotas;
    private javax.swing.JList<String> ListaMelhores;
    private javax.swing.JList<String> ListaMelhores1;
    private javax.swing.JList<String> ListaRecomendados;
    private javax.swing.JList<String> ListaUltimos;
    private javax.swing.JList<String> ListaUltimos1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenu2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    // End of variables declaration//GEN-END:variables
}
