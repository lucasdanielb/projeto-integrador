package classes;
/*
Está classe irá repesentar os Filmes do domínio.
*/
public class Movie {

    private int id;
    private String Nome;
    private String genero;

    public Movie() {

    }

    public Movie(int id, String nome, String genero) {
        this.id = id;
        this.Nome = nome;
        this.genero = genero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String toString() { //toString para personalização da exibição da classe Movie
        return this.Nome; //o que vai aparecer na comboBox  
    }
}
